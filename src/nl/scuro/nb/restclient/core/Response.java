/*
 * Copyright 2016 Mauro de Wit.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.scuro.nb.restclient.core;

import java.util.List;
import java.util.Map;

public class Response {

    private final int statusCode;
    private final String body, error;
    private final Map<String, List<String>> responseHeaders;

    public Response(int statusCode, String body, Map<String, List<String>> responseHeaders) {
        this.statusCode = statusCode;
        this.body = body;
        this.error = null;
        this.responseHeaders = responseHeaders;
    }
    
    public Response(String error, Map<String, List<String>> responseHeaders) {
        this.statusCode = -1;
        this.body = null;
        this.error = error;
        this.responseHeaders = responseHeaders;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "Response{" + "statusCode=" + statusCode + ", body=" + body + ", responseHeaders=" + responseHeaders + '}';
    }

    public String getError() {
        return error;
    }

}
