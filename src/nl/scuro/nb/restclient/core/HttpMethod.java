/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.scuro.nb.restclient.core;

/**
 *
 * @author mauro
 */
public enum HttpMethod {
    GET, POST, PUT, DELETE, HEAD;
}
