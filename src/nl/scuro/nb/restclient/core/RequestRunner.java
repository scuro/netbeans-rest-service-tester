/*
 * Copyright 2016 Mauro de Wit.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.scuro.nb.restclient.core;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import nl.scuro.nb.restclient.core.HttpClient;
import nl.scuro.nb.restclient.core.Response;

public class RequestRunner {

    String getLocation() {
        return location;
    }

    HttpMethod getHttpMethod() {
        return method;
    }

    Map<String, String> getHeaders() {
        return headers;
    }

    Map<String, Object> getParameters() {
        return params;
    }

    public String getBody() {
        return body;
    }

    private final HttpMethod method;
    private String location;
    private String body;
    private Map<String, String> headers;
    private Map<String, Object> params;
    
    public static RequestRunner withMethod(HttpMethod method, String url) {
        RequestRunner requestRunner = new RequestRunner(method);
        requestRunner.location = url;
        return requestRunner;
    }
    public static RequestRunner get(String url) {
        RequestRunner requestRunner = new RequestRunner(HttpMethod.GET);
        requestRunner.location = url;
        return requestRunner;
    }

    public static RequestRunner post(String url) {
        RequestRunner requestRunner = new RequestRunner(HttpMethod.POST);
        requestRunner.location = url;
        return requestRunner;
    }

    public static RequestRunner put(String url) {
        RequestRunner requestRunner = new RequestRunner(HttpMethod.PUT);
        requestRunner.location = url;
        return requestRunner;
    }
    
    public static RequestRunner delete(String url) {
        RequestRunner requestRunner = new RequestRunner(HttpMethod.DELETE);
        requestRunner.location = url;
        return requestRunner;
    }
    
    public static RequestRunner head(String url) {
        RequestRunner requestRunner = new RequestRunner(HttpMethod.HEAD);
        requestRunner.location = url;
        return requestRunner;
    }

    private RequestRunner(HttpMethod method) {
        this.method = method;
        this.headers = new HashMap<>();
        this.params = new HashMap<>();
    }

    public RequestRunner header(String key, String value) {
        this.headers.put(key, value);
        return this;
    }

    public RequestRunner headers(Map<String, String> headersMap) {
        this.headers = headersMap;
        return this;
    }

    public RequestRunner parameter(String key, String value) {
        this.params.put(key, value);
        return this;
    }

    public RequestRunner body(String body) {
        this.body = body;
        return this;
    }

    public Response run() {
        return HttpClient.executeRequest(this);
    }
}
