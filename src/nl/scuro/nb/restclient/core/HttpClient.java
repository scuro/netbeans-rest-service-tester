
/*
 * Copyright 2016 Mauro de Wit.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.scuro.nb.restclient.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.openide.util.Exceptions;

public class HttpClient {

    static TrustManager trm = new X509TrustManager() {
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {

        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    };

    static HostnameVerifier hostnameVerifier = new HostnameVerifier() {
        @Override
        public boolean verify(String string, SSLSession ssls) {
            return true;
        }
    };

    /**
     * @param args the command line arguments
     */
    public static Response executeRequest(RequestRunner runner) {

        try {
            String parameterString = createParameterString(runner.getParameters());
            URL url = new URL(runner.getLocation());
            HttpURLConnection conn = setupConnection(url);
            conn.setRequestMethod(runner.getHttpMethod().toString());
            addHeaders(runner.getHeaders(), conn);
            setBody(conn, runner.getBody());
            String result = getStringFromInputStream(conn.getInputStream());
            int responseCode = conn.getResponseCode();
            return new Response(responseCode, result, conn.getHeaderFields());
        } catch (KeyManagementException ex) {
            Exceptions.printStackTrace(ex);
            return new Response(ex.getMessage(), Collections.EMPTY_MAP);
        } catch (NoSuchAlgorithmException ex) {
            Exceptions.printStackTrace(ex);
            return new Response(ex.getMessage(), Collections.EMPTY_MAP);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
            return new Response(ex.getMessage(), Collections.EMPTY_MAP);
        }

    }

    private static HttpURLConnection setupConnection(URL url) throws KeyManagementException, NoSuchAlgorithmException, IOException {
        String protocol = url.getProtocol();
        if ("https".equalsIgnoreCase(protocol)) {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{trm}, null);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(hostnameVerifier);
            return conn;
        } else {
            return (HttpURLConnection) url.openConnection();
        }
    }

    private static void addHeaders(Map<String, String> headers, HttpURLConnection conn) {
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            conn.setRequestProperty(key, value);
        }
    }

    private static String createParameterString(Map<String, Object> params) throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder("?");
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            builder.append(key);
            builder.append('=');
            builder.append(value);
            builder.append('&');
        }
        return builder.toString();//URLEncoder.encode(builder.toString(), "UTF-8");
    }

    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

    private static void setBody(HttpURLConnection conn, String body) throws IOException {
        if (body == null || body.isEmpty()) {
            return;
        }
        conn.setDoOutput(true);
        final OutputStream outputStream = conn.getOutputStream();
        outputStream.write(body.getBytes());
        outputStream.close();
    }

}
