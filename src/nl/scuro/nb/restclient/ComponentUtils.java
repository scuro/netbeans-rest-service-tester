/*
 * Copyright 2016 Mauro de Wit.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.scuro.nb.restclient;

import java.util.Map;
import nl.scuro.nb.restclient.core.RequestRunner;
import nl.scuro.nb.restclient.core.Response;

public class ComponentUtils {

    public static Response runRequest(String url, String method, String input, Map<String, String> headers) {
        Response response = null;
        if ("POST".equals(method)) {
            response = RequestRunner.post(url).headers(headers).body(input).run();
        } else if ("PUT".equals(method)) {
            response = RequestRunner.put(url).headers(headers).body(input).run();
        } else if ("GET".equals(method)) {
            response = RequestRunner.get(url).headers(headers).run();
        } else if ("DELETE".equals(method)) {
            response = RequestRunner.delete(url).headers(headers).run();
        } else if ("HEAD".equals(method)) {
            response = RequestRunner.head(url).headers(headers).run();
        } else {
            throw new UnsupportedOperationException(method + " not supported.");
        }
        return response;
    }

    public static String formatResult(Response response) {

        StringBuilder sb = new StringBuilder();
        sb.append("HTTP Status code:");
        sb.append(response.getStatusCode());
        sb.append('\n');
        sb.append("Reponse headers:\n");
        sb.append(response.getResponseHeaders());
        sb.append('\n');
        sb.append("Reponse body:\n");
        sb.append(response.getBody());
        return sb.toString();
    }

}
