/*
 * Copyright 2016 Mauro de Wit.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nl.scuro.nb.restclient.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import org.openide.loaders.DataObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "Tools",
        id = "nl.scuro.nb.restclient.core.UseInRestClient"
)
@ActionRegistration(
        displayName = "#CTL_UseInRestClient"
)
@ActionReference(path = "Loaders/text/x-json/Actions", position = 1550, separatorBefore = 1525, separatorAfter = 1575)
@Messages("CTL_UseInRestClient=Input for Rest Client")
public final class UseInRestClient implements ActionListener {

    private final DataObject context;

    public UseInRestClient(DataObject context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        FileObject selectedFile = context.getPrimaryFile();
//        try {
            RestClientPanelTopComponent clientComponent = new RestClientPanelTopComponent();
            clientComponent.open();
//            clientComponent.setPostData(selectedFile.asText());
//        } catch (IOException ex) {
//            Exceptions.printStackTrace(ex);
//        }

    }
}
